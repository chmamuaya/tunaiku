import React from "react";
import { Row, Col, Card, Input, DatePicker } from "antd";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import FormikControl from "../components/FormikControl";
import "../assets/styles/registration.scss";

const RegistrationPage = (props) => {
  const { loan, term, amount } = props.location.state;

  const dropdownOptions = [
    { key: "Select an option", value: "" },
    { key: "Option 1", value: "option1" },
    { key: "Option 2", value: "option2" },
    { key: "Option 3", value: "option3" },
  ];
  const radioOptions = [
    { key: "Laki-laki", value: "laki-laki" },
    { key: "Perempuan", value: "perempuan" },
  ];
  const checkboxOptions = [
    { key: "Gaji", value: "gaji" },
    { key: "Bisnis", value: "bisnis" },
    { key: "Lainnya", value: "lainnya" },
  ];

  const initialValues = {
    name: "",
    phone: "",
    address: "",
    id_ktp: "",
    gender: "",
    birth_day: null,
    about_tnk: "",
    income: [],
  };

  const phoneRegex = /\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/;
  const uppercaseRegex = /(?=.*[A-Z])/;
  const numericRegex = /(?=.*[0-9])/;
  const alphabetRegex = /^[A-Za-z]+([\ A-Za-z]+)*/;

  const validationSchema = Yup.object({
    name: Yup.string()
      .matches(alphabetRegex, "Nama hanya boleh mengandung Alfabet")
      .required("Harus diisi!")
      .min(3, "Harus 3 karakter atau lebih!"),
    phone: Yup.string()
      .matches(phoneRegex, "Nomor tidak boleh mengandung angka 0 didepan")
      .required("Harus diisi!")
      .max(12, "Nomor tidak bisa lebih dari 12!"),
    address: Yup.string().required("Harus diisi!"),
    id_ktp: Yup.string().required("Harus diisi!").min(16, "Harus 16 karakter!"),
    gender: Yup.string().required("Harus diisi!"),
    birth: Yup.date().required("Harus diisi!"),
    about_tnk: Yup.string().required("Harus diisi!"),
    income: Yup.array().required("Harus diisi!"),
  });

  const onSubmit = (values) => {
    console.log(values);
    console.log("Saved data", JSON.parse(JSON.stringify(values)));
  };

  return (
    <div className="registration-form">
      <Row>
        <Col className="registration-form__card" span={12} offset={6}>
          <Card style={{ width: "100%" }}>
            <Card style={{ width: "100%", borderRadius: "10px" }}>
              <Row>
                <Col style={{ textAlign: "left", paddingBottom: "5%" }}>
                  <p>Total Pinjaman </p>
                  <span>{loan}</span>
                </Col>
              </Row>
              <Row>
                <Col flex={"50%"} style={{ textAlign: "left" }}>
                  <p>Lama Pinjaman </p>
                  <span>{term}</span>
                </Col>
                <Col flex={"50%"} style={{ textAlign: "right" }}>
                  <p>Cicilan per bulan </p>
                  <span>{amount}</span>
                </Col>
              </Row>
            </Card>
            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={onSubmit}
            >
              {(formik) => (
                <Form>
                  <FormikControl
                    control="input"
                    type="text"
                    label="Nama sesuai KTP*"
                    name="name"
                  />
                  <FormikControl
                    control="input"
                    type="number"
                    label="Nomor Handphone*"
                    name="phone"
                  />
                  <FormikControl
                    control="input"
                    type="text"
                    label="Alamat*"
                    name="address"
                  />
                  <FormikControl
                    control="input"
                    type="number"
                    label="Nomor KTP(NIK)*"
                    name="id_ktp"
                  />
                  <FormikControl
                    control="radio"
                    label="Jenis Kelamin*"
                    name="gender"
                    options={radioOptions}
                  />
                  <FormikControl
                    control="date"
                    label="Tanggal Lahir*"
                    name="birth_day"
                  />
                  <FormikControl
                    control="select"
                    label="Dari mana anda mengetahui Tunaiku*"
                    name="about_tnk"
                    options={dropdownOptions}
                  />
                  <FormikControl
                    control="checkbox"
                    label="Sumber Penghasilan Anda*"
                    name="income"
                    options={checkboxOptions}
                  />
                  <button type="submit">Submit</button>
                </Form>
              )}
            </Formik>
          </Card>
        </Col>
      </Row>
    </div>
  );
};
export default RegistrationPage;
