import React from "react";
import { Field, ErrorMessage } from "formik";
import TextError from "./TextError";

function InputNik(props) {
  const { label, name, ...rest } = props;
  return (
    <div className="form-control">
      <label htmlFor={name}>{label}</label>
      <Field id={name} name={name} {...rest} />
      <ErrorMessage component={TextError} name={name} />
    </div>
  );
}

export default InputNik;

// const MyField = (props) => {
//     const {
//       values: { textA, textB },
//       touched,
//       setFieldValue,
//     } = useFormikContext();
//     const [field, meta] = useField(props);

//     React.useEffect(() => {
//       // set the value of textC, based on textA and textB
//       if (
//         textA.trim() !== '' &&
//         textB.trim() !== '' &&
//         touched.textA &&
//         touched.textB
//       ) {
//         setFieldValue(props.name, `textA: ${textA}, textB: ${textB}`);
//       }
//     }, [textB, textA, touched.textA, touched.textB, setFieldValue, props.name]);

//     return (
//       <>
//         <input {...props} {...field} />
//         {!!meta.touched && !!meta.error && <div>{meta.error}</div>}
//       </>
//     );
//   };

// function OnBlurComponent({ onBlur }) {
//     const handleBlur = (e) => {
//       const currentTarget = e.currentTarget;

//       // Check the newly focused element in the next tick of the event loop
//       setTimeout(() => {
//         // Check if the new activeElement is a child of the original container
//         if (!currentTarget.contains(document.activeElement)) {
//           // You can invoke a callback or add custom logic here
//           onBlur();
//         }
//       }, 0);
//     };

//     return (
//       <div tabIndex="1" onBlur={onBlur}>
//         Hello <input type="text" value="world" />
//       </div>
//     );
//   }
