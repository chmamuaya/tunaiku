import React, { useState } from "react";
import Autosuggest from "react-autosuggest";
import { DatePicker as Date } from "antd";

const users = [
  {
    nickname: "crazyfrog",
    email: "frog@foobar.com",
  },
  {
    nickname: "tatanka",
    email: "ttt@hotmail.com",
  },
  {
    nickname: "wild",
    email: "www@mail.ru",
  },
  {
    nickname: "race car",
    email: "racing@gmail.com",
  },
  {
    nickname: "cook",
    email: "cooking@yahoo.com",
  },
];

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function getSuggestions(value) {
  const escapedValue = escapeRegexCharacters(value.trim());
  const regex = new RegExp("^" + escapedValue, "i");

  return users.filter(
    (user) => regex.test(user.nickname) || regex.test(user.email)
  );
}

function getSuggestionNickname(suggestion) {
  return suggestion.nickname;
}

function getSuggestionEmail(suggestion) {
  return suggestion.email;
}

function renderSuggestion(suggestion) {
  return (
    <span>
      {suggestion.nickname} - {suggestion.email}
    </span>
  );
}

function AutoFill(props) {
  const { setFieldValue } = props;

  const [nicknameValue, setNicknameValue] = useState("");
  const [nicknameSuggestions, setNicknameSuggestions] = useState([]);
  const [emailValue, setEmailValue] = useState("");
  const [emailSuggestions, setEmailSuggestions] = useState([]);

  const onNicknameChange = (event, { newValue }) => {
    setNicknameValue(newValue);
  };

  const onEmailChange = (event, { newValue }) => {
    setEmailValue(newValue);
  };

  const onNicknameSuggestionsFetchRequested = ({ value }) => {
    setNicknameSuggestions(getSuggestions(value));
  };

  const onNicknameSuggestionsClearRequested = () => {
    setNicknameSuggestions([]);
  };

  const onNicknameSuggestionSelected = (event, { suggestion }) => {
    setEmailValue(suggestion.email);
    setFieldValue("email", suggestion.email);
  };

  const onEmailSuggestionsFetchRequested = ({ value }) => {
    setEmailSuggestions(getSuggestions(value));
  };

  const onEmailSuggestionsClearRequested = () => {
    setEmailSuggestions([]);
  };

  const onEmailSuggestionSelected = (event, { suggestion }) => {
    setNicknameValue(suggestion.nickname);
    setFieldValue("nick", suggestion.nickname);
  };

  //   const nicknameInputProps = {
  //     placeholder: "nickname",
  //     value: nicknameValue,
  //     onChange: onNicknameChange,
  //   };
  //   const emailInputProps = {
  //     placeholder: "email",
  //     value: emailValue,
  //     onChange: onEmailChange,
  //   };

  return (
    <div className="container">
      <Autosuggest
        suggestions={nicknameSuggestions}
        onSuggestionsFetchRequested={onNicknameSuggestionsFetchRequested}
        onSuggestionsClearRequested={onNicknameSuggestionsClearRequested}
        onSuggestionSelected={onNicknameSuggestionSelected}
        getSuggestionValue={getSuggestionNickname}
        renderSuggestion={renderSuggestion}
        inputProps={{
          placeholder: "nickname",
          value: nicknameValue,
          name: "nick",
          onChange: onNicknameChange,
        }}
      />
      <Autosuggest
        suggestions={emailSuggestions}
        onSuggestionsFetchRequested={onEmailSuggestionsFetchRequested}
        onSuggestionsClearRequested={onEmailSuggestionsClearRequested}
        onSuggestionSelected={onEmailSuggestionSelected}
        getSuggestionValue={getSuggestionEmail}
        renderSuggestion={renderSuggestion}
        inputProps={{
          placeholder: "email",
          value: emailValue,
          name: "email",
          onChange: onEmailChange,
        }}
      />

      {/* <Autosuggest
        suggestions={emailSuggestions}
        onSuggestionsFetchRequested={onEmailSuggestionsFetchRequested}
        onSuggestionsClearRequested={onEmailSuggestionsClearRequested}
        onSuggestionSelected={onEmailSuggestionSelected}
        getSuggestionValue={getSuggestionEmail}
        renderSuggestion={renderSuggestion}
        inputProps={{
          placeholder: "email",
          value: emailValue,
          onChange: onEmailChange,
        }}
      /> */}
    </div>
  );
}

export default AutoFill;
