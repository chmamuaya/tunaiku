import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Card, Spin } from "antd";
import { Formik, Form } from "formik";
import { thousandSeparator } from "../../modules/utilities/thousand-separator.util";
import { FormikControl } from "../../modules/components/formik-control";
import { ArrowLeftOutlined } from "@ant-design/icons";
import "../../assets/styles/form.scss";
import {
  dropdownOptions,
  radioOptions,
  checkboxOptions,
  initialValues,
} from "./modules/constants/registration.const";
import { validationSchema } from "./modules/services/registration.validation";
import useRegistration from "./modules/services/registration.hook";
import { AMAR_BANK_COPYRIGHT } from "../../modules/constants/general.const";

function RegistrationPage(props) {
  const { loan, term, amount } = props.location.state;
  const { isLoading, onSubmit } = useRegistration(loan, term, amount);

  return (
    <div className="registration-form">
      <Row>
        <Col span={12} offset={6}>
          <Card>
            <Row>
              <Col flex={"20%"}>
                <Link to="/">
                  <ArrowLeftOutlined className="registration-form__arrow" />
                </Link>
              </Col>
              <Col
                className="registration-form__title-registration"
                flex={"80%"}
              >
                <h1>
                  <strong>Form Registrasi</strong>
                </h1>
              </Col>
            </Row>
            <Card className="registration-form__card">
              <Row>
                <Col className="registration-form__card-loan">
                  <p>Total Pinjaman </p>
                  <span>
                    Rp
                    {thousandSeparator(loan)}
                  </span>
                </Col>
              </Row>
              <Row>
                <Col flex={"50%"} className="registration-form__col-left">
                  <p>Lama Pinjaman </p>
                  <span>{term} bulan</span>
                </Col>
                <Col flex={"50%"} className="registration-form__col-right">
                  <p>Cicilan per bulan </p>
                  <span>
                    Rp
                    {thousandSeparator(amount)}
                  </span>
                </Col>
              </Row>
            </Card>
            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={onSubmit}
            >
              {({ isSubmitting }) => (
                <Form>
                  <FormikControl
                    control="input"
                    type="text"
                    label="Nama sesuai KTP*"
                    name="name"
                  />
                  <FormikControl
                    control="inputnumber"
                    type="text"
                    label="Nomor Handphone*"
                    name="phone"
                  />
                  <FormikControl
                    control="textarea"
                    label="Alamat*"
                    name="address"
                  />
                  <FormikControl
                    control="autofill"
                    label="Nomor KTP (NIK)*"
                    name="id_ktp"
                    type="text"
                  />
                  <Row>
                    <Col flex={"50%"} className="registration-form__col-center">
                      <FormikControl
                        control="radio"
                        label="Jenis Kelamin*"
                        name="gender"
                        options={radioOptions}
                      />
                    </Col>
                    <Col flex={"50%"}>
                      <FormikControl
                        control="date"
                        label="Tanggal Lahir*"
                        name="birth_day"
                      />
                    </Col>
                  </Row>
                  <FormikControl
                    control="checkbox"
                    label="Sumber Penghasilan Anda*"
                    name="income"
                    options={checkboxOptions}
                  />
                  <FormikControl
                    control="select"
                    label="Dari mana anda mengetahui Tunaiku*"
                    name="about_tnk"
                    options={dropdownOptions}
                  />
                  <button id="btn-submit" type="submit" disabled={isSubmitting}>
                    {isLoading === true ? (
                      <Spin style={{ color: "white" }} />
                    ) : (
                      "Lanjut Isi Formulir Pengajuan"
                    )}
                  </button>
                </Form>
              )}
            </Formik>
          </Card>
          <p id="footer">{AMAR_BANK_COPYRIGHT}</p>
        </Col>
      </Row>
    </div>
  );
}

export default RegistrationPage;
