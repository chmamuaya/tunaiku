import moment from "moment";
import "moment/locale/id";

export const dropdownOptions = [
  { key: "Pilih Salah Satu", value: "" },
  { key: "Internet", value: "internet" },
  { key: "Teman", value: "teman" },
  { key: "Radio", value: "radio" },
  { key: "Selebran/Brosur", value: "selebran/brosur" },
  { key: "Televisi", value: "televisi" },
  { key: "Lainnya", value: "Lainnya" },
];
export const radioOptions = [
  { key: "Laki-laki", value: "laki-laki" },
  { key: "Perempuan", value: "perempuan" },
];
export const checkboxOptions = [
  { key: "Gaji", value: "gaji" },
  { key: "Bisnis", value: "bisnis" },
  { key: "Lainnya", value: "lainnya" },
];

export const initialValues = {
  name: "",
  phone: "",
  address: "",
  id_ktp: "",
  gender: "",
  birth_day: moment("20/03/1998", "DD/MM/YYYY"),
  about_tnk: "",
  income: [],
};
