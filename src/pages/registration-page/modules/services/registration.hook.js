import { useState } from "react";
import { useHistory } from "react-router-dom";
import { PREFIX } from "../../../../modules/constants/product.const";

const useRegistration = (loan, term, amount) => {
  const [isLoading, setLoading] = useState(false);

  const history = useHistory();

  const onSubmit = (values, { setValues }) => {
    setLoading(true);

    setTimeout(() => {
      const prefix = PREFIX;
      const newphone = prefix + values.phone;
      const payload = { ...values, phone: newphone };
      setValues(payload);
      localStorage.setItem("payload2", JSON.stringify(payload));
    }, 500);

    setTimeout(() => {
      history.push({
        pathname: "/details",
        state: { loan: loan, term: term, amount: amount },
      });
    }, 2000);
  };

  return { isLoading, onSubmit };
};
export default useRegistration;
