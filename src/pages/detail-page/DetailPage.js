import React from "react";
import { Row, Col, Card } from "antd";
import moment from "moment";
import { AMAR_BANK_COPYRIGHT } from "../../modules/constants/general.const";
import { thousandSeparator } from "../../modules/utilities/thousand-separator.util";
import { COMA_REGEX } from "../../modules/constants/regex.const";

function DetailPage(props) {
  const { loan, term, amount } = props.location.state;
  const payload = localStorage.getItem("payload2");
  const details = JSON.parse(payload);

  return (
    <div className="detail-form">
      <Row>
        <Col span={12} offset={6}>
          <Card>
            <Row>
              <Col className="detail-form__title">
                <h1>
                  <strong>Detail Informasi</strong>
                </h1>
              </Col>
            </Row>
            <Card className="detail-form__card">
              <Row>
                <Col className="detail-form__card-loan">
                  <p>Total Pinjaman </p>
                  <span>
                    Rp
                    {thousandSeparator(loan)}
                  </span>
                </Col>
              </Row>
              <Row>
                <Col flex={"50%"} className="detail-form__col-left">
                  <p>Lama Pinjaman </p>
                  <span>{term} bulan</span>
                </Col>
                <Col flex={"50%"} className="detail-form__col-right">
                  <p>Cicilan per bulan </p>
                  <span>
                    Rp
                    {thousandSeparator(amount)}
                  </span>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col flex={"50%"} className="detail-form__col-left">
                  <p>Nama Sesuai KTP :</p>
                  <strong>{details.name}</strong>
                </Col>
                <Col flex={"50%"} className="detail-form__col-right">
                  <p>Nomor Handphone :</p>
                  <strong>{details.phone}</strong>
                </Col>
              </Row>
              <Row>
                <Col flex={"50%"} className="detail-form__col-left">
                  <p>Alamat :</p>
                  <strong>{details.address}</strong>
                </Col>
                <Col flex={"50%"} className="detail-form__col-right">
                  <p>Nomor KTP (NIK) :</p>
                  <strong>{details.id_ktp}</strong>
                </Col>
              </Row>
              <Row>
                <Col flex={"50%"} className="detail-form__col-left">
                  <p>Jenis Kelamin :</p>
                  <strong>{details.gender}</strong>
                </Col>
                <Col flex={"50%"} className="detail-form__col-right">
                  <p>Tanggal Lahir :</p>
                  <strong>
                    {moment(details.birth_day.slice(0, 10)).format("LL")}
                  </strong>
                </Col>
              </Row>
              <Row>
                <Col flex={"50%"} className="detail-form__col-left">
                  <p>Dari mana Anda mengetahui Tunaiku :</p>
                  <strong>{details.about_tnk}</strong>
                </Col>
                <Col flex={"50%"} className="detail-form__col-right">
                  <p>Sumber Penghasilan :</p>
                  {details.income.map((inc, index) => (
                    <strong>
                      {inc}{" "}
                      {index < details.income.length - 1 ? COMA_REGEX : ""}
                    </strong>
                  ))}
                </Col>
              </Row>
            </Card>
          </Card>
          <p id="footer">{AMAR_BANK_COPYRIGHT}</p>
        </Col>
      </Row>
    </div>
  );
}
export default DetailPage;
