import React from "react";
import { Row, Col } from "antd";
import "../../assets/styles/homepage.scss";
import { Slider } from "../../modules/components/slider";

const HomePage = () => {
  return (
    <div className="landing-hero">
      <Row>
        <Col span={12}>
          <Row>
            <div className="landing-hero__container--title margin-bottom">
              <h1 style={{ marginLeft: "15%", width: "100%" }}>
                Ajukan Pinjaman Online Tanpa Jaminan
              </h1>
            </div>
          </Row>
          <Row>
            <div className="landing-hero__container--desc margin-bottom">
              <p style={{ marginLeft: "16%", width: "100%" }}>
                {" "}
                Hanya dengan KTP, dapatkan kredit tanpa agunan hingga{" "}
                <strong>Rp20.000.000</strong>
              </p>
            </div>
          </Row>
        </Col>
        <Col span={4}>
          <div className="landing-hero__woman">
            <img
              className="landing-hero__woman--img"
              alt="tunaiku"
              src={require("../../assets/images/png/homepage-woman.png")}
            />
          </div>
        </Col>
        <Col span={8}>
          <Slider />
        </Col>
      </Row>
    </div>
  );
};
export default HomePage;
