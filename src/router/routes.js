import React from "react";
import { Route } from "react-router-dom";
import { Layout } from "antd";
import { NavBar } from "../modules/components/navbar";
import { HomePage } from "../pages/home-page";
import { RegistrationPage } from "../pages/registration-page";
import { DetailPage } from "../pages/detail-page";

const Routes = () => {
  return (
    <Layout>
      <header>
        <NavBar />
      </header>
      <div>
        <Route path="/" exact component={HomePage} />
        <Route path="/registration" exact component={RegistrationPage} />
        <Route path="/details" exact component={DetailPage} />
      </div>
      <footer></footer>
    </Layout>
  );
};

export default Routes;
