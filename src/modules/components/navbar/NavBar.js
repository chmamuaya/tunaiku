/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Menu, Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
import "../../../assets/styles/navbar.scss";

const NavBar = () => {
  const produk = (
    <Menu>
      <p>Nasabah</p>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="#">
          Referral
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="#">
          Swara
        </a>
      </Menu.Item>
      <hr></hr>
      <p>Bisnis</p>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="#">
          Partner Bisnis
        </a>
      </Menu.Item>
    </Menu>
  );
  const pusatbantuan = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="#">
          Tanya Jawab
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="#">
          Cara Pembayaran
        </a>
      </Menu.Item>
    </Menu>
  );
  return (
    <div className="tnk-navbar">
      <div className="tnk-navbar__container">
        <div className="master-brand tnk-navbar__block tnk-navbar__block--left">
          <a href="/">
            <img
              src={require("../../../assets/images/svg/tunaiku-logo-color.svg")}
              alt="logo tunaiku"
            />
          </a>
        </div>
        <div className="master-desktop tnk-navbar__block tnk-navbar__block--center">
          <div className="master-desktop__menu">
            <span>
              <img
                src={require("../../../assets/images/svg/tunaiku-invest-baru.svg")}
                alt="Tunaiku Invest"
              />
            </span>
            <a href="https://invest.tunaiku.com/redirect?utm_source=tunaiku&utm_medium=website&utm_campaign=new_product">
              Tunaiku Invest
            </a>
          </div>
          <div className="master-desktop__menu">
            <Dropdown overlay={produk}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Produk <DownOutlined />
              </a>
            </Dropdown>
          </div>
          <div className="master-desktop__menu">
            <a href="#">Tentang Kami</a>
          </div>
          <div className="master-desktop__menu">
            <a href="#">Rilis Media</a>
          </div>
          <div className="master-desktop__menu">
            <Dropdown overlay={pusatbantuan}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Pusat Bantuan <DownOutlined />
              </a>
            </Dropdown>
          </div>
        </div>
        <div className="master-help tnk-navbar__block">
          <span>
            <img
              src={require("../../../assets/images/svg/icon-comment.svg")}
              alt="icon saran tunaiku"
            />
          </span>
          <a href="#">Saran</a>
        </div>
        <div className="tnk-navbar__block tnk-navbar__block--right">
          <a href="#">
            <button className="tnk-button button-login">Masuk Akun</button>
          </a>
        </div>
      </div>
    </div>
  );
};
export default NavBar;
