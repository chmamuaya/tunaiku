import React from "react";
import { Field, ErrorMessage } from "formik";
import { Input } from "antd";
import { TextError } from "../text-error";
import { PREFIX } from "../../../constants/product.const";

function InputNumber(props) {
  const { label, name, ...rest } = props;

  return (
    <Field name={name}>
      {({ field }) => (
        <div className="form-control">
          <label htmlFor={name}>{label}</label>
          <Input addonBefore={PREFIX} id={name} {...rest} {...field} />
          <ErrorMessage component={TextError} name={name} />
        </div>
      )}
    </Field>
  );
}

export default InputNumber;
