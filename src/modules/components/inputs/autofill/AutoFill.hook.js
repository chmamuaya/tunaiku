import { useState } from "react";
import {
  ESCAPE_REGEX,
  MATCHED_STRING_REGEX,
} from "../../../constants/regex.const";

const useAutoFill = () => {
  const [suggestions, setSuggestions] = useState([]);

  const users = [
    {
      id_ktp: "1122334455667788",
      gender: "perempuan",
      birth_day: "20/03/1998",
    },
    {
      id_ktp: "1111222233334444",
      gender: "laki-laki",
      birth_day: "06/06/1998",
    },
    {
      id_ktp: "1234567891012345",
      gender: "laki-laki",
      birth_day: "06/12/1989",
    },
  ];

  function escapeRegexCharacters(str) {
    return str.replace(ESCAPE_REGEX, MATCHED_STRING_REGEX);
  }

  function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());
    const regex = new RegExp("^" + escapedValue, "i");

    return users.filter(
      (user) =>
        regex.test(user.id_ktp) ||
        regex.test(user.gender) ||
        regex.test(user.birth_day)
    );
  }
  return {
    suggestions,
    setSuggestions,
    getSuggestions,
  };
};
export default useAutoFill;
