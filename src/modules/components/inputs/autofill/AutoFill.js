import React from "react";
import { Field, ErrorMessage } from "formik";
import { TextError } from "../text-error";
import useAutoFill from "./AutoFill.hook";
import moment from "moment";

function AutoFill(props) {
  const { label, name, type, ...rest } = props;
  const { suggestions, setSuggestions, getSuggestions } = useAutoFill(props);

  return (
    <div className="form-control">
      <label htmlFor={name}>{label}</label>
      <Field
        id={name}
        name={name}
        {...rest}
        render={({ field, form }) => {
          const onTextChanged = (e) => {
            const value = e.target.value;
            let suggestions = [];
            if (value.length > 0) {
              suggestions = getSuggestions(value);
            }
            setSuggestions(suggestions);
            form.setFieldValue("id_ktp", value);
          };

          const handleBlur = (e) => {
            // programatically call `setFieldTouched` to make the field dirty so error message can be shown.
            form.setFieldTouched(field.name, true);

            if (form.values[e.target.value]) {
              form.setFieldValue(e.target.name);
            }
          };

          const inputProps = {
            ...field,
            label,
            name,
            type,
            onChange: onTextChanged,
            onBlur: handleBlur,
          };

          const renderSuggestions = () => {
            if (suggestions.length === 0) {
              return null;
            }
            return (
              <span className="registration-form__suggestion">
                {suggestions.map((item) => (
                  <p
                    onClick={() =>
                      setSuggestions([]) ||
                      form.setFieldValue(
                        "birth_day",
                        moment(item.birth_day, "DD/MM/YYYY")
                      ) &
                        form.setFieldValue("gender", item.gender) &
                        form.setFieldValue("id_ktp", item.id_ktp)
                    }
                  >
                    {" "}
                    {item.id_ktp} - {item.gender} - {item.birth_day}
                  </p>
                ))}
              </span>
            );
          };
          return (
            <div className="form-control">
              <input {...rest} {...inputProps} />
              {renderSuggestions()}
            </div>
          );
        }}
      />
      <ErrorMessage component={TextError} name={name} />
    </div>
  );
}

export default AutoFill;
