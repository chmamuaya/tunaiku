import { useState, useEffect } from "react";
import {
  ADMIN_FEE,
  DEFAULT_LOAN,
  DEFAULT_TERM,
} from "../../constants/product.const";

const useCalculationSlider = () => {
  const [loan, setLoan] = useState(DEFAULT_LOAN);
  const [term, setTerm] = useState(DEFAULT_TERM);
  const [amount, setAmount] = useState();

  const onChangeLoan = (value) => {
    setLoan(value);
  };

  const onChangeTerm = (value) => {
    setTerm(value);
  };

  function getLoanAmount() {
    var newloan = loan;
    var newterm = term;

    const installment = newloan / newterm; //installment per month
    const interest = (newloan * 3) / 100 / newterm; //3% interest rate
    const adminfee = ADMIN_FEE; //admin fee 150.000

    const newamount = installment + interest + adminfee;

    return newamount.toFixed(0);
  }

  useEffect(() => {
    setAmount(getLoanAmount);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loan, term]);

  return {
    loan,
    term,
    amount,
    onChangeLoan,
    onChangeTerm,
  };
};

export default useCalculationSlider;
