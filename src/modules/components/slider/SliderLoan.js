import React from "react";
import { Row, Col, Slider } from "antd";
import "./slider.scss";
import { Link } from "react-router-dom";
import useCalculationSlider from "./slider-loan.hook";
import { trimNumber } from "../../utilities/trim-number.util";
import { thousandSeparator } from "../../utilities/thousand-separator.util";
import {
  MAX_LOAN,
  MIN_LOAN,
  MAX_TERM,
  MIN_TERM,
  DEFAULT_LOAN,
  DEFAULT_TERM,
  STEP_LOAN,
  STEP_TERM,
} from "../../constants/product.const";

const SliderLoan = (props) => {
  const {
    loan,
    term,
    amount,
    onChangeLoan,
    onChangeTerm,
  } = useCalculationSlider(props);

  const marksloan = {
    2000000: "2 juta",
    20000000: {
      style: {
        color: "#3d3d3d",
      },
      label: <strong>20juta</strong>,
    },
  };
  const marksterm = {
    6: "6 bulan",
    20: {
      style: {
        color: "#3d3d3d",
      },
      label: <strong>20bulan</strong>,
    },
  };
  return (
    <div className="loan-slider">
      <div className="loan-slider__container">
        <Row>
          <Col flex={"50%"}>
            <p>Jumlah Pinjaman</p>
          </Col>
          <Col flex={"50%"}>
            <p className="loan-slider__container__loan">{trimNumber(loan)}</p>
          </Col>
        </Row>
        <Slider
          tooltipVisible={false}
          max={MAX_LOAN}
          min={MIN_LOAN}
          step={STEP_LOAN}
          defaultValue={DEFAULT_LOAN}
          marks={marksloan}
          onChange={onChangeLoan}
        />
        <br></br>
        <Row>
          <Col flex={"50%"}>
            <p>Lama Pinjaman</p>
          </Col>
          <Col flex={"50%"}>
            <p className="loan-slider__container__term">{term} bulan</p>
          </Col>
        </Row>
        <Slider
          tooltipVisible={false}
          max={MAX_TERM}
          min={MIN_TERM}
          step={STEP_TERM}
          defaultValue={DEFAULT_TERM}
          marks={marksterm}
          onChange={onChangeTerm}
        />
        <br></br>
        <hr></hr>
        <Row>
          <Col style={{ width: "100%", textAlign: "center" }}>
            <h3>
              <strong>Cicilan per bulan</strong>
            </h3>
            <p style={{ fontWeight: "200" }}>
              *Sudah termasuk bunga dan biaya admin
            </p>
            <p className="loan-slider__container__amount">
              {" "}
              Rp
              {thousandSeparator(amount)}
            </p>
          </Col>
        </Row>
        <Row>
          <Link
            to={{
              pathname: "/registration",
              state: {
                loan: loan,
                term: term,
                amount: amount,
              },
            }}
          >
            <button className="tnk-button full-width">Ajukan Pinjaman</button>
          </Link>
        </Row>
      </div>
    </div>
  );
};
export default SliderLoan;
