import React from "react";
import {
  Input,
  InputNumber,
  TextArea,
  Select,
  Radio,
  Checkbox,
  AutoFill,
} from "../inputs";
import { Date } from "../date";

// import Textarea from "./Textarea";
// import Select from "./Select";
// import RadioButtons from "./RadioButtons";
// import CheckboxGroup from "./CheckboxGroup";
// import DatePicker from "./DatePicker";

function FormikControl(props) {
  const { control, ...rest } = props;
  switch (control) {
    case "input":
      return <Input {...rest} />;
    case "inputnumber":
      return <InputNumber {...rest} />;
    case "autofill":
      return <AutoFill {...rest} />;
    case "textarea":
      return <TextArea {...rest} />;
    case "select":
      return <Select {...rest} />;
    case "radio":
      return <Radio {...rest} />;
    case "checkbox":
      return <Checkbox {...rest} />;
    case "date":
      return <Date {...rest} />;
    default:
      return null;
  }
}

export default FormikControl;
