import React from "react";
import { DatePicker as Date } from "antd";
import { useFormikContext } from "formik";
import { Field, ErrorMessage } from "formik";
import { TextError } from "../inputs/text-error";
import { FORMAT_DATE } from "../../constants/product.const";

const DatePicker = ({ ...props }) => {
  const { setFieldValue } = useFormikContext();
  const { label, name, ...rest } = props;

  return (
    <Field name={name}>
      {({ field }) => (
        <div className="form-control">
          <label htmlFor={name}>{label}</label>
          <Date
            format={FORMAT_DATE}
            id={name}
            name={name}
            {...field}
            {...rest}
            selected={(field.value && new Date(field.value)) || null}
            onChange={(val) => setFieldValue(name, val)}
          />
          <ErrorMessage component={TextError} name={name} />
        </div>
      )}
    </Field>
  );
};

export default DatePicker;
