export const TRIM_NUMBER_REGEX = /\.0$/;
export const ESCAPE_REGEX = /[.*+?^${}()|[\]\\]/g;
export const MATCHED_STRING_REGEX = "\\$&";
export const PHONE_REGEX = /\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/;
export const ALPHABET_REGEX = /^[aA-zZ\s]+$/;
export const NOZERO_REGEX = /^[1-9]\d*$/;
export const NUMERIC_REGEX = /(?=.*[0-9])/;
export const COMA_REGEX = ",\u00A0";

// const uppercaseRegex = /(?=.*[A-Z])/;
