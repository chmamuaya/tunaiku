export const FORMAT_DATE = "DD/MM/YYYY";
export const PREFIX = "+62";
export const MIN_LOAN = 2000000;
export const MAX_LOAN = 20000000;
export const DEFAULT_LOAN = 7000000;
export const STEP_LOAN = 1000000;
export const ADMIN_FEE = 150000;

export const MIN_TERM = 6;
export const MAX_TERM = 20;
export const DEFAULT_TERM = 12;
export const STEP_TERM = 1;
