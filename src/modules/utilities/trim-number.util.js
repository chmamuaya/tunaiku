import { TRIM_NUMBER_REGEX } from "../constants/regex.const";

export const trimNumber = (value) => {
  if (value >= 10000000) {
    return (
      (value / 1000000).toFixed(0).replace({ TRIM_NUMBER_REGEX }, "") + " Juta"
    );
  }
  if (value >= 1000000) {
    return (
      (value / 1000000).toFixed(0).replace({ TRIM_NUMBER_REGEX }, "") + " Juta"
    );
  }
  return value;
};
