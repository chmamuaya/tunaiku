export const thousandSeparator = (value) => {
  let separatedNumber;
  if (value !== "" && value !== null) {
    separatedNumber = Number(value).toLocaleString(["ban", "id"]);
  }
  return separatedNumber;
};
